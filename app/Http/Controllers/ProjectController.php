<?php namespace LabKanban\Http\Controllers;

use LabKanban\Http\Requests;
use LabKanban\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Vinkla\GitLab\Facades\GitLab;

class ProjectController extends Controller {

	public function getAll()
	{
		return response()->json(GitLab::api('projects')->accessible());
	}

}
