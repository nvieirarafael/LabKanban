<?php namespace LabKanban\Http\Controllers;

use Vinkla\GitLab\Facades\GitLab;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use LabKanban\User;

class UserController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function getSettings()
	{
		GitLab::authenticate(Auth::user()->gitlab_token);
		return view('user.settings');
	}

	public function postUpdate()
	{
		$user = User::find(Auth::id());
		$user->fill(Input::all());

		$message = ($user->save()) ? "Informações salvas" : "Houve algum problema";

		return redirect()->back()
										 ->with('messages', [$message]);
	}

}
