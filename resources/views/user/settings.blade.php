@extends('app')

@section('content')
<div class="content container">
  <h5 class="center-align">User Settings</h5>
  <div class="row">
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/update') }}">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">

      <div class="form-group">
        <label class="col-md-4 control-label">Name</label>
        <div class="col-md-6">
          <input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">E-Mail Address</label>
        <div class="col-md-6">
          <input type="email" class="form-control" name="email" value="{{ Auth::user()->email }}">
        </div>
      </div>

      <div class="form-group">
        <label class="col-md-4 control-label">GitLab Token</label>
        <div class="col-md-6">
          <input type="text" class="form-control" name="gitlab_token" value="{{ Auth::user()->gitlab_token }}">
        </div>
      </div>

      <div class="form-group">
        <button type="submit" class="btn teal right">
          Salvar <i class="mdi-content-save"></i>
        </button>
      </div>
    </form>
  </div>
</div>
@endsection
