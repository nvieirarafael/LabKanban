@extends('app')

@section('content')

<div class="content container">
	<h3 class="center-align login">Entre com suas informações</h3>
	<div class="row">
		<div class="col s6 offset-s3">
			@if (count($errors) > 0)
				<div class="card-panel red lighten-4">
        	<strong>Whoops!</strong> There were some problems with your input.
        	<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
        </div>
			@endif
			<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="input-field">
          <input name="email" type="email" class="validate">
          <label for="email">Email</label>
        </div>

				<div class="input-field">
          <input name="password" type="password" class="validate">
          <label for="password">Password</label>
        </div>

				<p>
	        <input type="checkbox" class="filled-in" id="remember" name="remember">
		      <label for="remember">Remember Me</label>
		     </p>
				{{-- </div> --}}

				<div class="input-field center-align">
					<button type="submit" class="waves-effect waves-light btn">Login</button>
					<a class="waves-effect waves-light btn" href="{{ url('/password/email') }}">Forgot Your Password?</a>
				</div>
			</form>
		</div>
	</div>
</div>


<div class="content container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">

				<div class="panel-body">



				</div>
			</div>
		</div>
	</div>
</div>
@endsection
