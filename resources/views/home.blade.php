@extends('app')

@section('content')
<div class="content container">
	<h5>Projetos</h5>
	<div class="row">
		@foreach ($projects as $project)
			<div class="card project">
        <div class="card-content">
          <span class="card-title grey-text text-darken-4">
          	{{ $project['name'] }} <a href="{{ $project['http_url_to_repo'] }}"><i class="mdi-action-find-in-page right small"></i></a>
          </span>
          <blockquote>
          	<p>{{ $project['description'] }}</p>
          </blockquote>
        </div>
      </div>
		@endforeach
	</div>
</div>
@endsection
