<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>LabKanban</title>

	<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper">
        <a href="/" class="brand-logo">LabKanban</a>
        <ul class="right hide-on-med-and-down">
          @if (Auth::guest())
						<li><a href="{{ url('/auth/login') }}">Login</a></li>
						<li><a href="{{ url('/auth/register') }}">Register</a></li>
					@else
            <li class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="User Settings">
              <a href="{{ url('/user/settings') }}"><i class="mdi-social-person"></i></a>
            </li>
            <li class="tooltipped" data-position="bottom" data-delay="50" data-tooltip="Logout">
              <a href="{{ url('/auth/logout') }}"><i class="mdi-action-exit-to-app"></i></a>
            </li>
            <li>
              <a class="user avatar" href="{{ Auth::user()->getGitLabPerfilURL() }}">
                <img src="{{ GitLab::api('users')->me()['avatar_url'] }}">
              </a>
            </li>
					@endif
        </ul>
      </div>
    </nav>
  </div>

	@yield('content')

  @if (session('messages') !== null)
    @foreach (session('messages') as $message)
      <span class="notification hide" data-message="{{ $message }}" data-class="teal"></span>
    @endforeach
  @endif

	<!-- Scripts -->
	<script type="text/javascript" src="{{ asset('/js/app.js') }}"></script>
</body>
</html>
