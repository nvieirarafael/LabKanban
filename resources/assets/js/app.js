(function ($) {
  $(document).ready(function () {

    $('.notification').each(function (index) {
      console.log(this);

      var message = $(this).attr('data-message');
      var clazz = $(this).attr('data-class');

      Materialize.toast(message, 4000, clazz + ' rounded');
    })

  });
}( jQuery ));
