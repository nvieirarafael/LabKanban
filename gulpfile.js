var elixir = require('laravel-elixir');

require('laravel-elixir-livereload');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

var paths = {
    'jquery': './vendor/bower_components/jquery/',
    'materialize': './vendor/bower_components/materialize/'
}

elixir(function(mix) {
    mix.livereload();

    mix.sass("app.scss", 'public/css/', {includePaths: [paths.materialize + 'sass/']})
        .copy(paths.materialize + 'dist/font/**', 'public/font')
        .scripts([
            paths.jquery + "dist/jquery.js",
            paths.materialize + "dist/js/materialize.min.js",
            "resources/assets/js/app.js",
        ], 'public/js/app.js', './');
});
